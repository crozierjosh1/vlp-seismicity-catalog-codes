function wf = gismo_load_sac(localdrive,starttemp,endtemp,ctags,responses)
% load from local sac files with GISMO
%josh crozier 2019

    %main folders contatining start+end points of traces
    startfolder1 = datestr(starttemp,'yyyymm');
    endfolder1 = datestr(endtemp,'yyyymm');
    %sub-folders
    if hour(starttemp) < 12
        ampm = '00';
    else
        ampm = '12';
    end
    startfolder2 = strcat(startfolder1,...
        datestr(starttemp,'dd'),'_',ampm,'0000_MAN');
    if hour(endtemp) < 12
        ampm = '00';
    else
        ampm = '12';
    end
    endfolder2 = strcat(endfolder1,...
        datestr(endtemp,'dd'),'_',ampm,'0000_MAN');

    %check for files in start folder
    dspath = strcat(localdrive,startfolder1,'/',startfolder2,'/');
    count = 0;
    validinds = [];
    wf = [];
    for chanin = 1:length(ctags)
        filename_temp = strcat(ctags(chanin).station,'.',...
            ctags(chanin).channel,'.',...
            ctags(chanin).network,'.');
        filepath_temp = strcat(dspath,filename_temp);
        isdata = true;
        if isfile(strcat(filepath_temp,'--'))
            %by default npt stations are unzipped have this title format
            sacds = datasource('sac', strcat(filepath_temp,'--'));
        elseif isfile(strcat(filepath_temp,'00'))
            %for files prevoiusly unzipped will have different format
            sacds = datasource('sac', strcat(filepath_temp,'00'));
        elseif isfile(strcat(filepath_temp,'00.gz'))
            %files that need unzipped
            gunzip(strcat(filepath_temp,'00.gz'))
            sacds = datasource('sac', strcat(filepath_temp,'00'));
        else
            isdata = false;
        end
        %load data
        if isdata
            count = count+1;
            validinds = [validinds,chanin];
            if count == 1
                wf = waveform(sacds, ctags(chanin), ...
                    datestr(starttemp, 31),...
                    datestr(endtemp, 31));
            else
                wf(count) = waveform(sacds, ctags(chanin), ...
                    datestr(starttemp, 31),...
                    datestr(endtemp, 31));
            end
            wf(count) = addfield(wf(count),'SACPZ',responses(chanin));
        end
    end
    
    %check for continued files in end folder
    if ~strcmp(startfolder2,endfolder2)
        keepind = true(size(wf));
        dspath = strcat(localdrive,endfolder1,'/',endfolder2,'/');
        for wfin = 1:length(validinds)
            chanin = validinds(wfin);
            filename_temp = strcat(ctags(chanin).station,'.',...
                ctags(chanin).channel,'.',...
                ctags(chanin).network,'.');
            filepath_temp = strcat(dspath,filename_temp);
            isdata = true;
            if isfile(strcat(filepath_temp,'--'))
                %by default npt stations are unzipped have this title format
                sacds = datasource('sac', strcat(filepath_temp,'--'));
            elseif isfile(strcat(filepath_temp,'00'))
                %for files prevoiusly unzipped will have different format
                sacds = datasource('sac', strcat(filepath_temp,'00'));
            elseif isfile(strcat(filepath_temp,'00.gz'))
                %files that need unzipped
                gunzip(strcat(filepath_temp,'00.gz'))
                sacds = datasource('sac', strcat(filepath_temp,'00'));
            else
                isdata = false;
            end
            %load data
            if isdata
               wftemp = waveform(sacds, ctags(chanin), ...
                    datestr(starttemp, 31),...
                    datestr(endtemp, 31));
                wf(wfin) = combine([wf(wfin),wftemp]);
            else
                keepind(wfin) = false;
            end
            
        end %end loop over channels
        wf = wf(keepind);
    end %end if different end folder
    
end %end function

