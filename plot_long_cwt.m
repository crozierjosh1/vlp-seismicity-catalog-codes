%plot long cwts
%have cwt_struct of choice in workspace
% load("TQ_20080101T000000_20111229T000000\cwt\cwt_2010.mat")
load("TQ_20120101T000000_20181230T000000\cwt\cwt_2013.mat")

divisions = 6;
edges = linspace(cwt_struct.time(1),cwt_struct.time(end),divisions + 1);

%scale by numchannels
% for ii = 1:size(cwt_struct.cwt,1)
%     cwt_struct.cwt(ii,:) = cwt_struct.cwt(ii,:)./cwt_struct.numchannels;
% end

%scale by minamp
minvec = quantile(cwt_struct.cwt,0.05,1);
for ii = 1:size(cwt_struct.cwt,1)
    cwt_struct.cwt(ii,:) = cwt_struct.cwt(ii,:) - minvec;
end

% %fill gaps
% dt = seconds(60);
% for ii = 1:length(cwt_struct.time)
%     
% end

%resample
Tsnew = seconds(60*20);
Ts = cwt_struct.time(2)-cwt_struct.time(1);
span = ceil(seconds(Tsnew)/seconds(Ts)/2)*2 + 1;
% cwt_struct.cwt = lowpass(cwt_struct.cwt',1/2*seconds(Tsnew),1/seconds(Ts));
% cwt_struct.cwt = cwt_struct.cwt';
cwt_struct.timeplot = cwt_struct.time(1):Tsnew:cwt_struct.time(end);
cwt_struct.cwtplot = NaN(size(cwt_struct.cwt,1),length(cwt_struct.timeplot));
for ii = 1:size(cwt_struct.cwt,1)
    cwt_struct.cwt(ii,:) = smooth(cwt_struct.cwt(ii,:), span, 'moving');
%     cwt_struct.cwtplot(ii,:) = resample(cwt_struct.cwt(ii,:),cwt_struct.time,...
%         1/seconds(Tsnew));
    cwt_struct.cwtplot(ii,:) = interp1(cwt_struct.time,cwt_struct.cwt(ii,:),...
        cwt_struct.timeplot);
end

p = {};
cwt_range = [quantile(cwt_struct.cwtplot(:),0.05),quantile(cwt_struct.cwtplot(:),0.95)];
figure('name','cwtplot')
for ii = 1:divisions
    subplot(divisions,1,ii)
    startind = find(cwt_struct.timeplot >= edges(ii),1);
    endind = find(cwt_struct.timeplot >= edges(ii+1),1)-1;
    if isempty(endind)
        endind = length(cwt_struct.timeplot);
    end
%     p{ii} = pcolor(cwt_struct.timeplot(startind:endind),1./cwt_struct.s,...
%         cwt_struct.cwtplot(:,startind:endind));
%     p{ii}.EdgeColor = 'none';
%     set(gca,'Yscale','log')
    imagesc(cwt_struct.cwtplot(:,startind:endind))
    ytickinds = (1:10:length(cwt_struct.s))';
    yticks(ytickinds)
    yticklabels(num2str(1./cwt_struct.s(ytickinds),'%.1f\n'))
    xlabels = cwt_struct.timeplot(startind:endind);
    xtickinds = (1:1000:length(xlabels))';
    xticks(xtickinds)
    xlabels = datestr(xlabels(xtickinds),6);
    xticklabels(xlabels)
    colormap(jet)
    caxis(cwt_range)
    ylabel('T (s)')
end