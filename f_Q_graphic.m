function f_Q_graphic(Ts, freal, Q, pcount, zcount, Ttarget,Qtarget)
%Josh Crozier 2020
%modified from Lesange 2008
%Graphics of the complex frequency plane

figure(8),clf

%Fn = 0.5/Ts;
%gmin = min(g);

% % Defines the lines of iso-values of Q
% x=0:.1:Fn;
% y1=-x/2;        % Q=1
% y2=-x/10;
% y3=-x/20;
% y4=-x/30;
% y5=-x/40;
% y6=-x/60;
% %y7=-x/80;
% y8=-x/100;
% y9=-x/200;        % Q=100
% %y10=-x/400;

scatter(1./freal,Q,16,pcount,'.')
hold on
scatter(1./freal,Q,16,zcount,'o')
legend({'color = num poles','color = Num zeros'},'AutoUpdate',false)
cbar = colorbar;
ylabel(cbar,'Pole/Zero Count')
colormap(cool)
plot(seconds(Ttarget),Qtarget,'ko','markersize',36)
plot(seconds(Ttarget),Qtarget,'k+','markersize',36)
% Plots the lines of iso-Q
%plot(1./x,y1,'g',1./x,y2,'g',1./x,y3,'g',1./x,y4,'g',1./x,y5,'g',1./x,y6,'g',1./x,y8,'g',1./x,y9,'g');

xlabel('T (s)','FontSize',8,'Fontweight','bold');
ylabel('Q','Fontweight','bold');
%axis([ 0 Fn -.5 0]);        % This limit in g is somewhat arbitrary...
%axis([ 0 Fn gmin 0]);
%axis([ 0 4 -0.04 0]);
set(gca,'fontsize',8,'Fontweight','bold','TickLength',[0.01 0.025]);
set(gca,'XScale','log')
set(gca,'YScale','log')
grid on
title('AR Method')

end %end function