function U = mogi_greensfunc(mdl,obs,mu,nu)
%coded by josh crozier 6/12/2018
%mogi greens function
%outputs displacement U
%same input format as disloc3d okada code, except:
    %mdl = [sourcedepth sourceeast sourcenorth dV]
%functions from Segall EQ and Volcano deformation pg 206-207

%dV
dV = mdl(4);

%source loc
sx = mdl(2);
sy = mdl(3);
d = mdl(1);

%obs locations
ox = obs(1,:);
oy = obs(2,:);
oz = obs(3,:);

%source dist from obs (along surface)
rho = sqrt((sx-ox).^2 + (sy-oy).^2);

%displacements
U = zeros(size(obs));

%radial
U_rho = (1-nu)*dV/pi*rho./(rho.^2 + d^2).^(3/2);

%east
U(1,:) = U_rho.*(ox-sx)./rho;

%north
U(2,:) = U_rho.*(oy-sy)./rho;

%vertical
U(3,:) = (1-nu)*dV/pi*d./(rho.^2 + d^2).^(3/2);
end

