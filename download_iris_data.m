function [output] = download_iris_data(startdatetimestr)
%download seismic data from iris using irisfetch

% make sure the java jar is in the path, this need only be done once per MATLAB session
%javaaddpath("IRIS-WS-2.0.18.jar");

%args: Net, Sta, Loc, Cha, Starttime, Endtime [,quality][,includePZ][,verbosity]
mytrace=irisFetch.Traces('IU','ANMO','00','BHZ','2010-02-27 06:30:00','2010-02-27 07:30:00');

end

