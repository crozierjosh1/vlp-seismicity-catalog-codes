function [MT] = MT_decomp(MT)
%decomposes timeseries of MT values in table form
iso = zeros(size(MT,1),1);
clvd = zeros(size(MT,1),1);
dc = zeros(size(MT,1),1);
scalarmoment = zeros(size(MT,1),1);
% rot_to_vert = [1,0,0; 0,cosd(-HVO_okadastruct.min_param.dip),sind(-HVO_okadastruct.min_param.dip); 0,-sind(-HVO_okadastruct.min_param.dip),cosd(-HVO_okadastruct.min_param.dip)];
% rot_to_EN = [sind(HVO_okadastruct.min_param.strike),-cosd(HVO_okadastruct.min_param.strike),0; cosd(HVO_okadastruct.min_param.strike),sind(HVO_okadastruct.min_param.strike),0; 0,0,1];
for i = 1:size(MT,1)
    M = [MT.MT11(i), MT.MT12(i), MT.MT13(i);...
        MT.MT12(i), MT.MT22(i), MT.MT23(i);...
        MT.MT13(i), MT.MT23(i), MT.MT33(i)];
%     Mi = rot_to_vert*disloc*rot_to_vert.';
%     Mi = rot_to_EN*Mi*rot_to_EN.';
%     M(:,:,i) = Mi;
    try
        eigenvalues = eigs(M);
        eigenvalues = sort(eigenvalues,'descend');
        iso(i) = sum(eigenvalues)/3;
        clvd(i) = 2*(eigenvalues(1) + eigenvalues(3) -2*eigenvalues(2))/3;
        dc(i) = (eigenvalues(1) - eigenvalues(3) - abs(eigenvalues(1) + eigenvalues(3) - 2*eigenvalues(2)));
        scalarmoment(i) = abs(iso(i)) + abs(clvd(i)) + dc(i);
        %iso(i) = iso(i)/scalarmoment(i);
        %clvd(i) = clvd(i)/scalarmoment(i);
        %dc(i) = dc(i)/scalarmoment(i);
    catch
        iso(i) = 0;
        clvd(i) = 0;
        dc(i) = 0;
        scalarmoment(i) = 0;
    end
end
MT.iso = iso;
MT.clvd = clvd;
MT.dc = dc;
MT.scalarmoment = scalarmoment;
end

