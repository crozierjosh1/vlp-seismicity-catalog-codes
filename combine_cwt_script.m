%combine_cwt_structs
clear
% foldername = 'TQ_20120101T000000_20181230T000000\cwt';
% year = [2012:2018];
foldername = 'TQ_20120101T000000_20181230T000000\cwt';
year = [2012:2018];
dt = seconds(60);

for yy = 1:length(year)
    disp(yy)
    clear cwt_struct
    datetimes = datetime(year(yy),1,1,0,0,0):hours(12):datetime(year(yy)+1,1,1,0,0,0);
    for jj = 1:length(datetimes)
        if exist(strcat(foldername,'/cwt_',datestr(datetimes(jj),30),'.mat'),'file') == 2
            cwt_struct_temp = load(strcat(foldername,'/cwt_',datestr(datetimes(jj),30),'.mat'));
            cwt_struct_temp = cwt_struct_temp.cwt_struct;
            if ~exist('cwt_struct','var')
                cwt_struct = cwt_struct_temp;
            else
    %             gap = cwt_struct_temp.time(start) - cwt_struct.time(end);
    %             gaplen = round(seconds(gap/seconds(dt))) - 1;
                timegap = cwt_struct.time(end):dt:cwt_struct_temp.time(1);
                if length(timegap)>2
                    timegap = timegap(2:end-1);
                    cwtgap = NaN(size(cwt_struct.cwt,1),length(timegap));
                    numchannelgap = NaN(1,length(timegap));
                else
                    timegap = [];
                    cwtgap = double.empty(size(cwt_struct.cwt,1),0);
                    numchannelgap = [];
                end
                cwt_struct.time = [cwt_struct.time, timegap,...
                    cwt_struct_temp.time];
                cwt_struct.cwt = [cwt_struct.cwt, cwtgap...
                    cwt_struct_temp.cwt];
                cwt_struct.numchannels = [cwt_struct.numchannels, numchannelgap,...
                    cwt_struct_temp.numchannels+zeros(1,length(cwt_struct_temp.time))];
            end
        end
    end

    if ~isempty(cwt_struct)
        save(strcat(foldername,'/cwt_',...
            num2str(year(yy))), 'cwt_struct')
    end

end