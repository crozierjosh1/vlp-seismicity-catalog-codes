%% Specify Master stations db
dbMaster = '/aerun/sum/db/dbmaster/master_stations';

%% Load data
% startTime = datenum(2009,2,15,19,33,20);
% endTime = datenum(2009,2,15,19,40,0);
% ds = datasource('uaf_continuous');
% nslc(1) = scnlobject('COLA','BHZ'); % Has sampling rate of 20 Hz
% nslc(2) = scnlobject('RSO','EHZ'); % Has sampling rate of 100 Hz
% w=waveform(ds, nslc, startTime, endTime);

ds = datasource('irisdmcws');
ctag = ChannelTag('AV', 'RSO', '--', 'EHZ');
startTime = '2009/03/22 06:00:00';
endTime = '2009/03/22 07:00:00';
w = waveform(ds, ctag, startTime, endTime)

%% Clean data
w = medfilt1(w,3); % remove any spikes of sample length 1
w = fillgaps(w, 'interp');
w = detrend(w);

%% Downsample to a common sampling frequency
target_fsamp = 20; % samples per second
for c=1:numel(w)
    current_fsamp = round(get(w(c),'freq'));
    w(c) = resample(w(c), 'mean', current_fsamp/target_fsamp );
end

%% Deconvolve instrument response from AEC Master stations database
% response_apply was written by Mike
filterObj = filterobject('b',[0.5 5],2);
wFilt = filtfilt(filterObj,w);
wCorrected = response_apply(wFilt,filterObj,'antelope',dbMaster);

%% Plot result
plot_panels(wCorrected)