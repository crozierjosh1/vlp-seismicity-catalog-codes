function [traces,keepind] = local_HV_locations(traces)
%add locations to traces structure for Hawaii stations
%josh crozier 2020


keepind = true(size(traces));
for jj = 1:length(traces)
    if ~strcmp(traces(jj).network, 'HV')
        keepind(jj) = false;
    elseif strcmp(traces(jj).station,'KKO')
        traces(jj).latitude = 19.39781;
        traces(jj).longitude = -155.26600;
        traces(jj).elevation = 1146.0;
    elseif strcmp(traces(jj).station,'BYL')
        traces(jj).latitude = 19.41200;
        traces(jj).longitude = -155.26000;
        traces(jj).elevation = 1059.0;
    elseif strcmp(traces(jj).station,'HAT')
        traces(jj).latitude = 19.42300;
        traces(jj).longitude = -155.26100;
        traces(jj).elevation = 1082.0;
    elseif strcmp(traces(jj).station,'SBL')
        traces(jj).latitude = 19.42700;
        traces(jj).longitude = -155.26800;
        traces(jj).elevation = 1084.0;
    elseif strcmp(traces(jj).station,'UWB')
        traces(jj).latitude = 19.42469;
        traces(jj).longitude = -155.27800;
        traces(jj).elevation = 1091.0;
    elseif strcmp(traces(jj).station,'NPT')
        traces(jj).latitude = 19.41200;
        traces(jj).longitude = -155.28100;
        traces(jj).elevation = 1115.0;
    elseif strcmp(traces(jj).station,'OBL')
        traces(jj).latitude = 19.41750;
        traces(jj).longitude = -155.28400;
        traces(jj).elevation = 1107.0;
    elseif strcmp(traces(jj).station,'UWE')
        traces(jj).latitude = 19.42111;
        traces(jj).longitude = -155.29300;
        traces(jj).elevation = 1240.0;
    elseif strcmp(traces(jj).station,'SDH')
        traces(jj).latitude = 19.38950;
        traces(jj).longitude = -155.29400;
        traces(jj).elevation = 1133.0;
    elseif strcmp(traces(jj).station,'WRM')
        traces(jj).latitude = 19.40650;
        traces(jj).longitude = -155.30000;
        traces(jj).elevation = 1163.0;
    elseif strcmp(traces(jj).station,'RIMD')
        traces(jj).latitude = 19.39531;
        traces(jj).longitude = -155.27400;
        traces(jj).elevation = 1128.0;
    elseif strcmp(traces(jj).station,'PUHI')
        traces(jj).latitude = 19.385510;
        traces(jj).longitude = -155.251310;
        traces(jj).elevation = 1079;
    else
        keepind(jj) = false;
    end
    
end

end %end function

