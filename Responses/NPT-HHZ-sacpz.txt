* **********************************
* NETWORK   (KNETWK): HV
* STATION    (KSTNM): NPT
* LOCATION   (KHOLE):   
* CHANNEL   (KCMPNM): HHZ
* CREATED           : 2019-11-20T21:13:37
* START             : 2007-04-01T00:00:00
* END               : 2014-04-07T00:00:00
* DESCRIPTION       : North Pit
* LATITUDE          : 19.412033
* LONGITUDE         : -155.280922
* ELEVATION         : 1128.0
* DEPTH             : 0.0
* DIP               : 0.0
* AZIMUTH           : 0.0
* SAMPLE RATE       : 100.0
* INPUT UNIT        : M
* OUTPUT UNIT       : COUNTS
* INSTTYPE          : CMG-40T,VELOCITY-TRANSDUCER,GURALP
* INSTGAIN          : 8.000000e+02 (M/S)
* COMMENT           : 
* SENSITIVITY       : 5.033040e+08 (M/S)
* A0                : 5.714040e+08
* **********************************
ZEROS	3
	+0.000000e+00	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
POLES	5
	-7.401600e-02	+7.401600e-02	
	-7.401600e-02	-7.401600e-02	
	-5.026500e+02	+0.000000e+00	
	-1.005000e+03	+0.000000e+00	
	-1.131000e+03	+0.000000e+00	
CONSTANT	2.875899e+17


