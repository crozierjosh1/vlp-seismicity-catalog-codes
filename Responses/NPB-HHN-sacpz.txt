* **********************************
* NETWORK   (KNETWK): HV
* STATION    (KSTNM): NPB
* LOCATION   (KHOLE):   
* CHANNEL   (KCMPNM): HHN
* CREATED           : 2019-11-20T19:15:03
* START             : 2000-01-00T00:00:00
* END               : 2018-12-00T00:00:00
* DESCRIPTION       : From Phil Dawson, only poles/zeros/constant correct
* LATITUDE          : 19.0
* LONGITUDE         : -155.0
* ELEVATION         : 1000.0
* DEPTH             : 0.0
* DIP               : 90.0
* AZIMUTH           : 0.0
* SAMPLE RATE       : 100.0
* INPUT UNIT        : M
* OUTPUT UNIT       : COUNTS
* INSTTYPE          : CMG-40T,GURALP:CMG-40T:T4865
* INSTGAIN          : 1.000000e+00 (M/S)
* COMMENT           : From Phil Dawson, only poles/zeros/constant correct
* SENSITIVITY       : 1.000000e+00 (M/S)
* A0                : 1.000000e+00
* **********************************
ZEROS	3
	+0.000000e+00	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
POLES	5
	-7.401600e-02	+7.401600e-02	
	-7.401600e-02	-7.401600e-02	
	-5.026500e+02	+0.000000e+00	
	-1.005000e+03	+0.000000e+00	
	-1.131000e+03	+0.000000e+00	
CONSTANT	2.033055e+15
