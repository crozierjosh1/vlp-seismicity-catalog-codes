* **********************************
* NETWORK   (KNETWK): HV
* STATION    (KSTNM): SBL
* LOCATION   (KHOLE): 
* CHANNEL   (KCMPNM): HHZ
* CREATED           : 2020-02-13T04:24:43
* START             : 2017-03-30T00:00:00
* END               : 
* DESCRIPTION       : 
* LATITUDE          : 19.427078
* LONGITUDE         : -155.267685
* ELEVATION         : 1081.0
* DEPTH             : 0.0
* DIP               : 90.0
* AZIMUTH           : 0.0
* SAMPLE RATE       : 100.0
* INPUT UNIT        : M
* OUTPUT UNIT       : COUNTS
* INSTTYPE          : TRILLIUM COMPACT,VELOCITY-TRANSDUCER,NANOMETRICS
* INSTGAIN          : 7.491000e+02 (M)
* COMMENT           : 
* SENSITIVITY       : 4.712810e+08 (M)
* A0                : 8.198430e+11
* **********************************
ZEROS	4
	+0.000000e+00	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
	-4.341000e+02	+0.000000e+00	
POLES	7
	-3.691000e-02	+3.712000e-02	
	-3.691000e-02	-3.712000e-02	
	-3.712000e+02	+0.000000e+00	
	-3.739000e+02	+4.755000e+02	
	-3.739000e+02	-4.755000e+02	
	-5.884000e+02	+1.508000e+03	
	-5.884000e+02	-1.508000e+03	
CONSTANT	3.863764e+20


* **********************************
* NETWORK   (KNETWK): HV
* STATION    (KSTNM): SBL
* LOCATION   (KHOLE): 
* CHANNEL   (KCMPNM): HHZ
* CREATED           : 2020-02-13T04:24:43
* START             : 2011-07-06T00:00:00
* END               : 2017-03-30T00:00:00
* DESCRIPTION       : 
* LATITUDE          : 19.427078
* LONGITUDE         : -155.267685
* ELEVATION         : 1081.0
* DEPTH             : 0.0
* DIP               : 0.0
* AZIMUTH           : 0.0
* SAMPLE RATE       : 100.0
* INPUT UNIT        : M
* OUTPUT UNIT       : COUNTS
* INSTTYPE          : CMG-40T,VELOCITY-TRANSDUCER,GURALP
* INSTGAIN          : 8.000000e+02 (M)
* COMMENT           : 
* SENSITIVITY       : 5.033030e+08 (M)
* A0                : 5.714040e+08
* **********************************
ZEROS	3
	+0.000000e+00	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
POLES	5
	-7.401600e-02	+7.401600e-02	
	-7.401600e-02	-7.401600e-02	
	-5.026500e+02	+0.000000e+00	
	-1.005000e+03	+0.000000e+00	
	-1.131000e+03	+0.000000e+00	
CONSTANT	2.875893e+17


* **********************************
* NETWORK   (KNETWK): HV
* STATION    (KSTNM): SBL
* LOCATION   (KHOLE): 
* CHANNEL   (KCMPNM): HHZ
* CREATED           : 2020-02-13T04:24:43
* START             : 1998-02-01T00:00:00
* END               : 2011-07-06T00:00:00
* DESCRIPTION       : 
* LATITUDE          : 19.427078
* LONGITUDE         : -155.267685
* ELEVATION         : 1081.0
* DEPTH             : 0.0
* DIP               : 0.0
* AZIMUTH           : 0.0
* SAMPLE RATE       : 100.0
* INPUT UNIT        : M
* OUTPUT UNIT       : COUNTS
* INSTTYPE          : CMG-40T,VELOCITY-TRANSDUCER,GURALP
* INSTGAIN          : 8.000000e+02 (M)
* COMMENT           : 
* SENSITIVITY       : 3.558000e+06 (M)
* A0                : 5.714040e+08
* **********************************
ZEROS	3
	+0.000000e+00	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
POLES	5
	-7.401600e-02	+7.401600e-02	
	-7.401600e-02	-7.401600e-02	
	-5.026500e+02	+0.000000e+00	
	-1.005000e+03	+0.000000e+00	
	-1.131000e+03	+0.000000e+00	
CONSTANT	2.033055e+15




