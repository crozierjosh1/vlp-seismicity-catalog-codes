* **********************************
* NETWORK   (KNETWK): HV
* STATION    (KSTNM): WRM
* LOCATION   (KHOLE):   
* CHANNEL   (KCMPNM): HHE
* CREATED           : 2019-11-20T21:13:38
* START             : 1998-02-01T00:00:00
* END               : 2011-07-08T00:00:00
* DESCRIPTION       : West Rim Hawaii Digital
* LATITUDE          : 19.406623
* LONGITUDE         : -155.300005
* ELEVATION         : 1153.0
* DEPTH             : 0.0
* DIP               : 90.0
* AZIMUTH           : 90.0
* SAMPLE RATE       : 100.0
* INPUT UNIT        : M
* OUTPUT UNIT       : COUNTS
* INSTTYPE          : CMG-40T,VELOCITY-TRANSDUCER,GURALP
* INSTGAIN          : 8.000000e+02 (M/S)
* COMMENT           : 
* SENSITIVITY       : 3.558000e+06 (M/S)
* A0                : 5.714040e+08
* **********************************
ZEROS	3
	+0.000000e+00	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
POLES	5
	-7.401600e-02	+7.401600e-02	
	-7.401600e-02	-7.401600e-02	
	-5.026500e+02	+0.000000e+00	
	-1.005000e+03	+0.000000e+00	
	-1.131000e+03	+0.000000e+00	
CONSTANT	2.033055e+15


