* **********************************
* NETWORK   (KNETWK): HV
* STATION    (KSTNM): RIMD
* LOCATION   (KHOLE): 
* CHANNEL   (KCMPNM): HHZ
* CREATED           : 2020-02-13T04:25:55
* START             : 2012-06-05T00:00:00
* END               : 
* DESCRIPTION       : 
* LATITUDE          : 19.395369
* LONGITUDE         : -155.273636
* ELEVATION         : 1145.0
* DEPTH             : 0.0
* DIP               : 90.0
* AZIMUTH           : 0.0
* SAMPLE RATE       : 100.0
* INPUT UNIT        : M
* OUTPUT UNIT       : COUNTS
* INSTTYPE          : TRILLIUM 120P,NANOMETRICS:TRILLIUM 120P:1040
* INSTGAIN          : 1.201000e+03 (M)
* COMMENT           : 
* SENSITIVITY       : 7.555840e+08 (M)
* A0                : 3.090360e+05
* **********************************
ZEROS	6
	+0.000000e+00	+0.000000e+00	
	-9.000000e+01	+0.000000e+00	
	-1.607000e+02	+0.000000e+00	
	-3.108000e+03	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
POLES	7
	-3.852000e-02	-3.658000e-02	
	-3.852000e-02	+3.658000e-02	
	-1.780000e+02	+0.000000e+00	
	-1.350000e+02	-1.600000e+02	
	-1.350000e+02	+1.600000e+02	
	-6.710000e+02	-1.154000e+03	
	-6.710000e+02	+1.154000e+03	
CONSTANT	2.335027e+14


* **********************************
* NETWORK   (KNETWK): HV
* STATION    (KSTNM): RIMD
* LOCATION   (KHOLE): 
* CHANNEL   (KCMPNM): HHZ
* CREATED           : 2020-02-13T04:25:55
* START             : 2010-11-15T00:00:00
* END               : 2012-06-05T00:00:00
* DESCRIPTION       : 
* LATITUDE          : 19.395369
* LONGITUDE         : -155.273636
* ELEVATION         : 1145.0
* DEPTH             : 0.0
* DIP               : 0.0
* AZIMUTH           : 0.0
* SAMPLE RATE       : 100.0
* INPUT UNIT        : M
* OUTPUT UNIT       : COUNTS
* INSTTYPE          : TRILLIUM 120P,NANOMETRICS:TRILLIUM 120P:1040
* INSTGAIN          : 1.201000e+03 (M)
* COMMENT           : 
* SENSITIVITY       : 7.555850e+08 (M)
* A0                : 3.090360e+05
* **********************************
ZEROS	6
	+0.000000e+00	+0.000000e+00	
	-9.000000e+01	+0.000000e+00	
	-1.607000e+02	+0.000000e+00	
	-3.108000e+03	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
	+0.000000e+00	+0.000000e+00	
POLES	7
	-3.852000e-02	-3.658000e-02	
	-3.852000e-02	+3.658000e-02	
	-1.780000e+02	+0.000000e+00	
	-1.350000e+02	-1.600000e+02	
	-1.350000e+02	+1.600000e+02	
	-6.710000e+02	-1.154000e+03	
	-6.710000e+02	+1.154000e+03	
CONSTANT	2.335030e+14




