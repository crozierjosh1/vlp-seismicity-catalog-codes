﻿This DATSETNAMEreadme.txt file was generated on 2021-1-25 by Josh Crozier


GENERAL INFORMATION

1. Title of Dataset: VLP seismic event catalogs and catalog generation codes corresponding to manuscript "Very-long-period seismicity over the 2008-2018 eruption of Kilauea Volcano"

2. Author Information
	A. Principal Contact Information
		Name: Josh Crozier
		Institution: University of Oregon
		Email: jcrozier@uoregon.edu or crozierjosh1@gmail.com

	B. Alternate Contact Information
		Name: Leif Karlstrom
		Institution: University of Oregon
		Email: leif@uoregon.edu


3. Date of data collection (single date, range, approximate date): 
2008-01-01 to 2018-08-01

4. Geographic location of data collection: 
Kilauea Volcano, Hawaii, USA

5. Information about funding sources that supported the collection of the data: 
Seismic data used to make these catalogs is from the USGS HVO network
Leif Karlstrom acknowledges support for this work from NSF EAR-1624557


SHARING/ACCESS INFORMATION

1. Licenses/restrictions placed on the data:
Public Domain/CC-0

2. Links to publications that cite or use the data: 
Very-long-period seismicity over the 2008-2018 eruption of Kilauea Volcano
preprint: https://www.essoar.org/doi/10.1002/essoar.10504113.2

3. Links to other publicly accessible locations of the data: 
https://bitbucket.org/crozierjosh1/vlp-seismicity-catalog-codes/src/master/

4. Links/relationships to ancillary data sets: 
None

5. Was data derived from another source? 
yes, USGS HVO seismic network

6. Recommended citation for this dataset: 
please cite the publication "Very-long-period seismicity over the 2008-2018 eruption of Kilauea Volcano"


DATA & FILE OVERVIEW

1. File List: 
Kilauea_2008-2018_resonant_signal_catalog_presented.csv: A version of our event catalog thresholded to include 3209 events, as presented in the text. The first row contains the names of each variable, more detailed variable descriptions are provided below.
    
Kilauea_2008-2018_resonant_signal_catalog_full.csv: A version of our event catalog thresholded to include 33084 events. The thresholds used in this version are: STA/LTA > 2, standard deviations above the LTA > 1, $Q$ > 4, and mean phase deviation < 0.25 radians. The first row contains the names of each variable, more detailed variable descriptions are provided below.

catalog_analysis.m: Matlab script loads and plots data from the Kilauea VLP catalog presented in the publication.

T_Q_sepdownload.m: Matlab script is the main script that makes VLP seismicity catalogs. By default it will download data from IRIS for HVO seismometers near Kilauea, though it should work for any seismic data available on IRIS. The included Gismo toolbox will be needed. The included GISMO toolbox has minor modifications from the main version related to the downloading of instrument responses, and to the printing of console text (so that it doesn't waste time printing millions of lines of text during each data download). As of publication time the irisfetch functionality for automatically downloading instrument resonses from IRIS alongside seismic data was broken, so the script currently is manually obtaining instrument responses for the HVO stations from the local 'responses' folder. The IRIS website states that they are working on a fix for this. Some functionality is built in to also load data from local SAC files.

GISMO_JoshCrozierMod.mltbx is a modified version of the GISMO matlab toolbox that may need to be installed for some of the codes to work. The GISMO-master folder just contains files from this modified GISMO toolbox. Other versions of the GISMO toolbox may work; the main modifications included here is a reduction of messages printed to the terminal (decreasing computation times), and options to obtain instrument responses from IRIS along with each waveform (though as of early 2020 IRIS had changed something that broke this functionality in the irisfetch program so these lines might be commented out in the GISMO files included here).

Responses folder contains instrument response information for the seismometers used in this study

All other files are helper functions.


2. Relationship between files, if important: 
see file descriptions above

3. Additional related data collected that was not included in the current data package: 
None

4. Are there multiple versions of the dataset? 
yes, see file descriptions above


METHODOLOGICAL INFORMATION

1. Description of methods used for collection/generation of data: 
see publication "Very-long-period seismicity over the 2008-2018 eruption of Kilauea Volcano"
preprint: https://www.essoar.org/doi/10.1002/essoar.10504113.2

2. Methods for processing the data: 
see publication "Very-long-period seismicity over the 2008-2018 eruption of Kilauea Volcano"
preprint: https://www.essoar.org/doi/10.1002/essoar.10504113.2

3. Instrument- or software-specific information needed to interpret the data: 
Matlab r2020, Matlab Wavelet Toolbox, (optional) Matlab Parallel Computing Toolbox
(codes were tested on both a Windows 10 PC and linux based Talapas HPC https://hpcf.uoregon.edu/content/talapas)

4. Standards and calibration information, if appropriate: 
NA

5. Environmental/experimental conditions: 
NA

6. Describe any quality-assurance procedures performed on the data: 
see publication "Very-long-period seismicity over the 2008-2018 eruption of Kilauea Volcano"
preprint: https://www.essoar.org/doi/10.1002/essoar.10504113.2

7. People involved with sample collection, processing, analysis and/or submission: 
Josh Crozier and Leif Karlstrom


DATA-SPECIFIC INFORMATION FOR: [FILENAME]
Kilauea_2008-2018_resonant_signal_catalog_presented.csv and
Kilauea_2008-2018_resonant_signal_catalog_full.csv

1. Number of variables: 
160

2. Number of cases/rows: 
3210 for "presented", 33085 for "full"

3. Variable List: 
T	Period (s)
stdamp	standard deviation of stacked cwt amp over window before event onset log10 (m/s)
meanamp	mean stacked cwt amp over window before event onset log10 (m/s)
medamp	median stacked cwt amp over window before event onset log10 (m/s)
minamp	min stacked cwt amp over window before event onset log10 (m/s)
maxamp	max stacked cwt amp over window before event onset log10 (m/s)
rangeamp	range of stacked cwt amp over window before event onset log10 (m/s)
prom	prominence of event onset in stacked cwt log10 (m/s)
start	event onset datetime (UTC)
Acwt	stacked cwt amp at event onset log10 (m/s)
Q	quality factor
g	decay rate
rsquare	r-squared value for exponential decay fit
dfe	degrees of freedom for error in exponentia decay fit
adjrsquare	adjusted r-squared value for exponential decay fit
rmse	root mean square error for exponential decay fit
windowstartTime	start datetime of data window used to make scaleograms (UTC)
channelCount	total number of channels used
amp_HV_NPB_HHE	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_NPB_HHN	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_NPB_HHZ	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_SRM_HHE	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_SRM_HHN	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_SRM_HHZ	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_NPT_HHE	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_NPT_HHN	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_NPT_HHZ	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_OBL_HHE	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_OBL_HHN	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_OBL_HHZ	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_WRM_HHE	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_WRM_HHN	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_WRM_HHZ	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_SDH_HHE	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_SDH_HHN	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_SDH_HHZ	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_UWE_HHE	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_UWE_HHN	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_UWE_HHZ	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_UWB_HHE	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_UWB_HHN	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_UWB_HHZ	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_SBL_HHE	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_SBL_HHN	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_SBL_HHZ	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_KKO_HHE	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_KKO_HHN	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_KKO_HHZ	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_RIMD_HHE	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_RIMD_HHN	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
amp_HV_RIMD_HHZ	amplitude (m/s) averaged over 4 cycles for 'network_station_channel'
phase_HV_NPB_HHE	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_NPB_HHN	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_NPB_HHZ	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_SRM_HHE	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_SRM_HHN	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_SRM_HHZ	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_NPT_HHE	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_NPT_HHN	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_NPT_HHZ	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_OBL_HHE	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_OBL_HHN	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_OBL_HHZ	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_WRM_HHE	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_WRM_HHN	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_WRM_HHZ	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_SDH_HHE	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_SDH_HHN	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_SDH_HHZ	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_UWE_HHE	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_UWE_HHN	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_UWE_HHZ	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_UWB_HHE	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_UWB_HHN	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_UWB_HHZ	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_SBL_HHE	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_SBL_HHN	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_SBL_HHZ	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_KKO_HHE	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_KKO_HHN	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_KKO_HHZ	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_RIMD_HHE	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_RIMD_HHN	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
phase_HV_RIMD_HHZ	phase (radians) calculated over 4 cycles, relative to event onset time, for 'network_station_channel'
errphase_HV_NPB_HHE	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_NPB_HHN	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_NPB_HHZ	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_SRM_HHE	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_SRM_HHN	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_SRM_HHZ	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_NPT_HHE	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_NPT_HHN	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_NPT_HHZ	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_OBL_HHE	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_OBL_HHN	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_OBL_HHZ	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_WRM_HHE	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_WRM_HHN	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_WRM_HHZ	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_SDH_HHE	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_SDH_HHN	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_SDH_HHZ	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_UWE_HHE	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_UWE_HHN	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_UWE_HHZ	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_UWB_HHE	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_UWB_HHN	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_UWB_HHZ	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_SBL_HHE	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_SBL_HHN	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_SBL_HHZ	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_KKO_HHE	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_KKO_HHN	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_KKO_HHZ	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_RIMD_HHE	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_RIMD_HHN	mean deviation in phase (radians) from expected,  for 'network_station_channel'
errphase_HV_RIMD_HHZ	mean deviation in phase (radians) from expected,  for 'network_station_channel'
mean_errphase	mean mean phase deviation over all channels
mean_min50_errphase	mean mean phase deviation over best half of channels (in case some are distorted by high noise)
fm_start	picked first motion datetime (UTC)
fm_A_above_LTA	stacked amplitude (m/s) at picked first motion peak minus stacked amplitude over preceding window 
fm_STALTA	stacked amplitude (m/s) at picked first motion peak relative to stacked amplitude over preceding window 
fm_Afrac	stacked amplitude (m/s) at picked first motion peak relative to next highest stacked amplitude peak in time window around event onset (!!!might not have saved this value correctly!!!)
fm_HV_NPB_HHE	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_NPB_HHN	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_NPB_HHZ	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_SRM_HHE	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_SRM_HHN	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_SRM_HHZ	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_NPT_HHE	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_NPT_HHN	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_NPT_HHZ	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_OBL_HHE	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_OBL_HHN	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_OBL_HHZ	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_WRM_HHE	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_WRM_HHN	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_WRM_HHZ	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_SDH_HHE	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_SDH_HHN	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_SDH_HHZ	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_UWE_HHE	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_UWE_HHN	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_UWE_HHZ	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_UWB_HHE	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_UWB_HHN	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_UWB_HHZ	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_SBL_HHE	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_SBL_HHN	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_SBL_HHZ	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_KKO_HHE	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_KKO_HHN	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_KKO_HHZ	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_RIMD_HHE	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_RIMD_HHN	velocity (m/s) at picked first motion time,  for 'network_station_channel'
fm_HV_RIMD_HHZ	velocity (m/s) at picked first motion time,  for 'network_station_channel'
Acwt_norm	stacked cwt amplitude at event onset time normalized by total number of channels log10 (m/s)
amp_abovenoise	stacked cwt amplitude at event onset minus average amplitude over preceding window log10 (m/s)
std_abovenoise	number of standard deviations that stacked cwt amplitude at event onset is above average stacked cwt amplitude plus standard deviation for preceding window (unitless)
std_abovemed	number of standard deviations that stacked cwt amplitude at event onset is above median stacked cwt amplitude plus standard deviation for preceding window (unitless)


4. Missing data codes: 
"NaN"

5. Specialized formats or other abbreviations used: 
NA
