function f_g_graphic(Ts, freal, g)
%Josh Crozier 2020
%modified from Lesange 2008
%Graphics of the complex frequency plane

figure(7),clf

Fn = 0.5/Ts;
%gmin = min(g);

% Defines the lines of iso-values of Q
x=0:.1:Fn;
y1=-x/2;        % Q=1
y2=-x/10;
y3=-x/20;
y4=-x/30;
y5=-x/40;
y6=-x/60;
%y7=-x/80;
y8=-x/100;
y9=-x/200;        % Q=100
%y10=-x/400;

plot(1./freal,g,'bo','MarkerSize',6)
hold on
% Plots the lines of iso-Q
plot(1./x,y1,'g',1./x,y2,'g',1./x,y3,'g',1./x,y4,'g',1./x,y5,'g',1./x,y6,'g',1./x,y8,'g',1./x,y9,'g');

xlabel('T (s)','FontSize',8,'Fontweight','bold');
ylabel('g (s^-1)','Fontweight','bold');
text(1./1,-.47,'Q=1','FontSize',8,'color','g');
text(1./4.5,-.43,'5','FontSize',8,'color','g');
text(1./9,-.42,'10','FontSize',8,'color','g');
text(1./9.1,-.28,'15','FontSize',8,'color','g');
text(1./9.3,-.21,'20','FontSize',8,'color','g');
text(1./9.5,-.13,'30','FontSize',8,'color','g');
text(1./9.6,-.07,'50','FontSize',8,'color','g');
text(1./9.6,-.03,'100','FontSize',8,'color','g');
text(1./(0.97*Fn),-0.05,'Complex frequency','Fontsize',8,'HorizontalAlignment','right','Fontweight','bold')
ylim([-0.4,0])
%axis([ 0 Fn -.5 0]);        % This limit in g is somewhat arbitrary...
%axis([ 0 Fn gmin 0]);
%axis([ 0 4 -0.04 0]);
set(gca,'fontsize',8,'Fontweight','bold','TickLength',[0.01 0.025]);
set(gca,'XScale','log')

end %end function